import tkinter as tk
import tkinter.messagebox

from a2_solution import *
from constants import *

TASK1_BAR_HEIGHT = 50


class AbstractGrid(tk.Canvas):
    def __init__(self, master, rows, cols, width, height, **kwargs):
        tk.Canvas.__init__(self, master, width=cols * width, height=rows * height, **kwargs)
        self.rows = rows
        self.cols = cols
        self.width = width
        self.height = height
        self.pack(side=tk.LEFT)

    def get_box(self, position):
        """
        Returns the bounding box for the (row, column) position.
        :param position:
        :return:
        """
        box = (position.get_x() * self.width, position.get_y() * self.height, (
                position.get_x() + 1) * self.width, (position.get_y() + 1) * self.height)
        # return (i + MY_OFFSET for i in box)
        return box

    def pixel_to_position(self, pixel):
        """
        Converts the (x, y) pixel position (in graphics units)
        to a (row, column) position.
        :param self:
        :param pixel:
        :return:
        """
        return Position(pixel[0] // self.width, pixel[1] // self.height)

    def get_position_center(self, position):
        """
        Get the graphics coordinates for the center of the cell at the given
        (row, column) position.
        :param position:
        :return:
        """
        center = (position.get_x() + 0.5) * self.width, (position.get_y() + 0.5) * self.height
        # return Position(*(i + MY_OFFSET for i in center))
        # center = Position((position[0] + 0.5) * self.width, (position[1] + 0.5) * self.height)
        return center

    def draw_rectangle(self, position, **kwargs):
        """

        :param position:
        :param kwargs:
        :return:
        """
        self.create_rectangle(*self.get_box(position), **kwargs)

    def annotate_position(self, position, text, **kwargs):
        """
        Annotate the center of the cell at the given (row, column) position
        with the provided text.
        :param position:
        :param text:
        :return:
        """
        center = self.get_position_center(position)
        self.create_text(center[0], center[1], text=text, **kwargs)


class BasicMap(AbstractGrid):
    def __init__(self, master, size, **kwargs):
        super().__init__(master, size, size, width=CELL_SIZE, height=CELL_SIZE, **kwargs)

    def draw_entity(self, position, tile_type):
        """
        Draw the entity.
        :param position:
        :param tile_type:
        :return:
        """
        self.create_rectangle(*self.get_box(position),
                              fill=ENTITY_COLOURS[tile_type],
                              )
        self.annotate_position(position, text=tile_type,
                               fill='white' if tile_type in (PLAYER, HOSPITAL) else DARK_PURPLE,
                               )


class InventoryView(AbstractGrid):
    def __init__(self, master, rows, **kwargs):
        super().__init__(master, rows, 1, width=INVENTORY_WIDTH, height=CELL_SIZE, **kwargs)

    def draw(self, inventory):
        """
        Draw the inventory label and current items with their remaining lifetimes.
        :param inventory:
        :return:
        """
        # Draw title
        self.create_rectangle(*self.get_box(Position(0, 0)), outline=LIGHT_PURPLE)
        self.annotate_position(Position(0, 0), text='Inventory',
                               fill=DARK_PURPLE, font=('Arial', 15, 'normal'))

        start_col = 1
        for item in inventory.get_items():
            if isinstance(item, Garlic):
                text = 'Garlic'
            elif isinstance(item, Crossbow):
                text = 'Crossbow'
            else:
                return  # should never happen
            if item.is_active():
                text_color, bg_color = LIGHT_PURPLE, DARK_PURPLE
            else:
                text_color, bg_color = DARK_PURPLE, LIGHT_PURPLE
            if item.get_lifetime():
                self.draw_rectangle(Position(0, start_col), outline=bg_color, fill=bg_color)
                self.create_text(self.width / 4, (start_col * 2 + 1) * self.height / 2,
                                 text=text, fill=text_color, font=('Arial', 10, 'normal'))
                self.create_text(3 * self.width / 4, (start_col * 2 + 1) * self.height / 2,
                                 text=str(item.get_lifetime()), fill=text_color, font=('Arial', 10, 'normal'))
                start_col += 1

    def toggle_item_activation(self, pixel, inventory):
        """
        Activates or deactivates the item (if one exits) in the row containing the pixel.
        :param pixel:
        :param inventory:
        :return:
        """
        click_position = self.pixel_to_position(pixel)
        current_items = inventory.get_items()
        if click_position.get_y() > len(current_items):
            return
        click_item = current_items.pop(click_position.get_y() - 1)
        click_item.toggle_active()
        for item in current_items:
            if item.is_active():
                item.toggle_active()
        self.draw(inventory)


class BasicGraphicalInterface:
    def __init__(self, root, size):
        """
        Init BasicGraphicalInterface
        :param root: the root window
        :param size: represents the number of rows
        """
        self.root = root
        self.size = size

    def _inventory_click(self, event, inventory):
        """
        Handle left clicks.
        :param event:
        :param inventory:
        :return:
        """
        self.inventory_view.toggle_item_activation((event.x, event.y), inventory)

    def handle_action(self, event, game) -> None:
        """
        Handle direction for game map.
        :param event:
        :param game:
        :return:
        """
        action = event.char.upper()
        if action in DIRECTIONS:
            direction = action
            self._move(game, direction)
        else:
            action = event.keysym.title()
            if action in ARROWS_TO_DIRECTIONS:
                direction = ARROWS_TO_DIRECTIONS.get(action)
                player = game.get_player()
                if player is None or not isinstance(player, HoldingPlayer):
                    return  # Should never happen.
                inventory = player.get_inventory()
                if inventory.contains(CROSSBOW) and inventory.has_active(CROSSBOW):
                    start = game.get_grid().find_player()
                    offset = game.direction_to_offset(direction)
                    if start is None or offset is None:
                        return  # Should never happen.
                    first = first_in_direction(
                        game.get_grid(), start, offset
                    )

                    # If the entity is a zombie, kill it.
                    if first is not None and first[1].display() in ZOMBIES:
                        position, entity = first
                        game.get_grid().remove_entity(position)
                    else:
                        print(NO_ZOMBIE_MESSAGE)
                else:
                    print(NO_WEAPON_MESSAGE)
            else:
                print(INVALID_FIRING_MESSAGE)

    def _create_game_map(self, root, game):
        """
        Draw game map.
        :param root:
        :param game: The game instance
        :return:
        """
        if hasattr(self, 'game_map_canvas'):
            self.game_map_canvas.delete('all')
        else:
            self.game_map_canvas = BasicMap(root, self.size, bg=LIGHT_BROWN)
        for position, entity in game.get_grid().get_mapping().items():
            self.game_map_canvas.draw_entity(position, entity.display())

    def _create_inventory(self, root, game) -> None:
        """
        Draw inventory view.
        :param game: The game instance
        :return:
        """
        if hasattr(self, 'inventory_view'):
            self.inventory_view.delete('all')
        else:
            self.inventory_view = InventoryView(root, self.size, bg=LIGHT_PURPLE)
        player = game.get_player()
        if isinstance(player, Player):
            inventory = game.get_player().get_inventory()
            self.inventory_view.draw(inventory)
            self.inventory_view.bind('<Button-1>', lambda e: self._inventory_click(e, inventory))

    def draw(self, game: Game):
        """
        Clears and redraws the view based on the current game state.
        :param game: The game instance.
        :return:
        """
        head_width = self.size * CELL_SIZE + INVENTORY_WIDTH
        heading_label_canvas = tk.Canvas(self.root, width=head_width, height=TASK1_BAR_HEIGHT)
        heading_label_canvas.pack(side=tk.TOP)
        heading_label_canvas.create_rectangle(0, 0, head_width, TASK1_BAR_HEIGHT, fill=DARK_PURPLE)
        heading_label_canvas.create_text(head_width / 2, TASK1_BAR_HEIGHT / 2, text=TITLE, fill='white',
                                         font=('Arial', 18, 'normal'))
        self._create_game_map(self.root, game)
        self._create_inventory(self.root, game)

    def _move(self, game, direction):
        """
        Handles moving the player and redrawing the game.
        :param game: The game instance
        :param direction: Character representing the direction in which the player should be moved.
        :return:
        """
        offset = game.direction_to_offset(direction)
        if offset is not None:
            game.move_player(offset)
            game.step()
        self._create_game_map(self.root, game)
        if game.has_won():
            print(WIN_MESSAGE)
            tk.messagebox.showinfo('Message', WIN_MESSAGE)
            self.root.quit()
        elif game.has_lost():
            print(LOSE_MESSAGE)
            tk.messagebox.showinfo('Message', LOSE_MESSAGE)
            self.root.quit()
        else:
            return

    def _step(self, game):
        """
        This method triggers the step method for the game and updates the view accordingly.
        :param game:
        :return:
        """
        player = game.get_player()
        if isinstance(player, Player):
            inventory = game.get_player().get_inventory()
            if inventory.any_active():
                inventory.step()
        self._create_game_map(self.root, game)
        self._create_inventory(self.root, game)
        self.game_map_canvas.after(1000, lambda: self._step(game))

    def play(self, game):
        """
        Binds events and initialises gameplay.
        :param game: An instance of the game class that is to be displayed
                     to the user by printing the grid.
        :return:
        """
        self.draw(game)
        self.root.focus_set()
        self.root.bind('<KeyPress >', lambda e: self.handle_action(e, game))
        self.root.after(1000, self._step(game))
        self.root.mainloop()
