import tkinter as tk
from PIL import Image
from PIL import ImageTk

from a2_solution import *
from constants import *
from task1 import BasicGraphicalInterface, BasicMap

STATUSBAR_HEIGHT = 75
SAVED_GRID = {}
RESTARTING = False
WIN_OR_LOSE = None
BEGIN_COUNT_TIME = False
CAN_MOVE = True
PROMPT_POPUP = None


def load_image(path, resize=None):
    """

    :param path:
    :param resize:
    :return:
    """
    image = Image.open(path)
    if resize is not None:
        image = image.resize(resize)
    return ImageTk.PhotoImage(image)


def load_high_scores() -> List:
    """

    """
    high_scores = []
    try:
        with open(HIGH_SCORES_FILE, 'r', encoding='utf-8') as f:
            for line in f.readlines():
                player_name, score = line.strip().split(': ')
                high_scores.append([player_name, int(score)])
    except Exception as e:
        print(e)

    high_scores.sort(key=lambda x: x[1])
    return high_scores[:3]


def reload_game(game, grid=None):
    """

    :param game:
    :return:
    """
    if grid is None:
        grid = SAVED_GRID
    global WIN_OR_LOSE, RESTARTING, BEGIN_COUNT_TIME, CAN_MOVE
    for position, entity in game.get_grid().get_mapping().items():
        game.get_grid().remove_entity(position)

    for p, e in grid.items():
        game.get_grid().add_entity(Position(*p), AdvancedMapLoader().create_entity(e))

    game._player_position = game.get_grid().find_player()
    game._steps = 0

    RESTARTING = False
    WIN_OR_LOSE = None
    BEGIN_COUNT_TIME = False
    CAN_MOVE = True


class GameRecord:
    def __init__(self, path):
        self._path = path
        self.sep = '=' * 10 + '\n'
        self._load(path)

    def _parse(self, mapping_str, inventory_str, steps_timer_str):
        """

        :param mapping_str:
        :param inventory_str:
        :param steps_timer_str:
        :return:
        """
        mapping = {}
        inventory = []
        for line in mapping_str.splitlines():
            p, e = line.strip().split('|')
            position = (int(i) for i in p.split(','))
            mapping[position] = e
        inventory_str = inventory_str.strip()
        if inventory_str:
            for i in inventory_str.strip().split('|'):
                inventory.append(i.split(','))
        steps, timer = steps_timer_str.split('|')
        self._mapping = mapping
        self._inventory = inventory
        self._steps = steps
        self._timer = timer

    def _load(self, path):
        if path.endswith('.eodz'):
            with open(path, 'r') as f:
                try:
                    mapping_str, inventory_str, steps_timer_str = f.read().split(self.sep)
                    self._parse(mapping_str, inventory_str, steps_timer_str)
                except Exception as e:
                    inform = tk.Toplevel()
                    inform.attributes('-toolwindow', 1)
                    inform.wm_attributes("-topmost", 1)
                    tk.Label(inform, text='Something went wrong! Load default map').pack(fill=tk.BOTH)
                    inform.title('Non-game file')
                    width, height = 400, 20
                    screenwidth = inform.winfo_screenwidth()
                    screenheight = inform.winfo_screenheight()
                    inform.geometry(f'{width}x{height}+'
                                    f'{(screenwidth - width) // 2}+{(screenheight - height) // 2}')
                    inform.after(2000, inform.destroy)
                    with open(r'saves\0.eodz', 'r') as f:
                        mapping_str, inventory_str, steps_timer_str = f.read().split(self.sep)
                    with open(path, 'w') as f:
                        f.write(self.sep.join([mapping_str, inventory_str, steps_timer_str]))
                    self._parse(mapping_str, inventory_str, steps_timer_str)
        else:
            print('Trying to load a non-game file!')

    @property
    def mapping(self):
        return self._mapping

    @property
    def inventory(self):
        return self._inventory

    @property
    def steps(self):
        return self._steps

    @property
    def timer(self):
        return self._timer

    def save(self, mapping, inventory, steps, timer):
        with open(self._path, 'w') as f:
            for position, entity in mapping.items():
                p = ','.join([str(position.get_x()), str(position.get_y())])
                e = entity.display()
                line = '|'.join([p, e]) + '\n'
                f.write(line)
            f.write(self.sep)
            f.write('|'.join([str(i)[0] + ',' + str(i.get_lifetime()) for i in inventory.get_items()]) + '\n')
            f.write(self.sep)
            f.write('|'.join([str(steps), str(timer)]))


class Banner(tk.Frame):
    def __init__(self, master, width, height, banner_image):
        tk.Frame.__init__(self, master, width=width, height=height)

        heading_label_canvas = tk.Canvas(self, width=width, height=height)
        heading_label_canvas.pack(side=tk.TOP)
        heading_label_canvas.create_image(0, 0, anchor=tk.NW, image=banner_image)
        self.pack(side=tk.TOP)


class StatusBar(tk.Frame):
    def __init__(self, master, width, height):
        tk.Frame.__init__(self, master, width=width, height=height)
        self.chaser_image = load_image(r'images\chaser.png')
        self.chasee_image = load_image(r'images\chasee.png')
        self.pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)
        self.timer = tk.StringVar()
        self.moves_counter = tk.StringVar()
        self.moves_counter.set('0 moves')
        self.font = ('Arial', 12, 'normal')
        self.game = None

    def restart_game(self):
        """
        Restart game.
        """
        global PROMPT_POPUP
        reload_game(self.game)
        if PROMPT_POPUP is not None:
            exec('PROMPT_POPUP.destroy()')

    def draw(self, game):
        """

        :return:
        """
        self.game = game
        chaser_label = tk.Label(self, image=self.chaser_image)
        chaser_label.pack(side=tk.LEFT, padx=30)
        # draw game timer frame
        game_timer_frame = tk.Frame(self)
        game_timer_frame.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)
        tk.Label(game_timer_frame, text='Timer', font=self.font).pack(side=tk.TOP, fill=tk.Y, expand=True)
        tk.Label(game_timer_frame, textvariable=self.timer, font=self.font).pack(side=tk.TOP, fill=tk.Y, expand=True)

        # draw move counter frame
        moves_counter_frame = tk.Frame(self)
        moves_counter_frame.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)
        tk.Label(moves_counter_frame, text='Moves made', font=self.font).pack(side=tk.TOP, fill=tk.Y, expand=True)
        tk.Label(moves_counter_frame, textvariable=self.moves_counter, font=self.font).pack(side=tk.TOP, fill=tk.Y,
                                                                                            expand=True)

        # draw control panel frame
        control_panel_frame = tk.Frame(self)
        control_panel_frame.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)
        tk.Button(control_panel_frame, text='Restart Game', font=self.font, command=self.restart_game).pack(side=tk.TOP,
                                                                                                            fill=tk.Y,
                                                                                                            expand=True)
        tk.Button(control_panel_frame, text='Quit Game', font=self.font, command=self.quit).pack(side=tk.TOP, fill=tk.Y,
                                                                                                 expand=True)

        chasee_label = tk.Label(self, image=self.chasee_image)
        chasee_label.pack(side=tk.RIGHT, padx=30)


class ImageMap(BasicMap):
    def __init__(self, master, size, **kwargs):
        super().__init__(master, size, **kwargs)

    def draw_background(self, image):
        """

        :param image:
        :return:
        """
        width, height = image.width(), image.height()
        rows = self.rows * self.height // height
        cols = self.cols * self.width // width
        for i in range(rows):
            for j in range(cols):
                self.create_image(j * width, i * height, anchor=tk.NW, image=image)

    def draw_entity(self, position, image):
        """
        Draw entity
        :param position:
        :param image:
        :return:
        """
        self.create_image(*self.get_position_center(position), image=image)


class ImageGraphicalInterface(BasicGraphicalInterface):
    def __init__(self, root, size):
        super().__init__(root, size)
        self.window_width = self.size * CELL_SIZE + INVENTORY_WIDTH
        self.window_height = BANNER_HEIGHT + self.size * CELL_SIZE + STATUSBAR_HEIGHT
        self.banner_image = load_image(r'images\banner.png', (self.window_width, BANNER_HEIGHT))
        self.back_ground_image = load_image(IMAGES['B'])
        self.time_counter = 0
        self.game = None

        self.prompt_popup = None
        self.prompt_width = 250
        self.prompt_height = 100

        self.high_scores_popup = None
        self.high_scores_width = 200
        self.high_scores_height = 150
        self.high_scores = load_high_scores()
        self.final_score = 0

        # self.game_records = [GameRecord(rf'saves\{i}.eodz') for i in range(1, 4)]
        self.game_records = [rf'saves\{i}.eodz' for i in range(1, 4)]

    def _create_game_map(self, root: tk.Tk, game: Game) -> None:
        """

        :param root:
        :param game:
        :return:
        """
        if hasattr(self, 'game_map_canvas'):
            self.game_map_canvas.delete('all')
        else:
            self.game_map_canvas = ImageMap(root, self.size)
        self.game_map_canvas.draw_background(self.back_ground_image)
        for position, entity in game.get_grid().get_mapping().items():
            setattr(self, str(position), load_image(IMAGES[entity.display()]))
            self.game_map_canvas.draw_entity(position, getattr(self, str(position)))

    def _create_banner(self, root):
        """

        :param root:
        :return:
        """
        Banner(root, width=self.window_width, height=BANNER_HEIGHT, banner_image=self.banner_image)

    def _create_status_bar(self, root, game):
        """

        :param root:
        :return:
        """
        self.status_bar = StatusBar(root, self.window_width, BANNER_HEIGHT // 2)
        self.status_bar.draw(game)

    def _restart_game(self):
        """
        Restart the game.
        :return:
        """
        reload_game(self.game)
        if self.prompt_popup is not None:
            self.prompt_popup.destroy()

    def _save_game(self, path):
        """

        :return:
        """
        if WIN_OR_LOSE is None:
            mapping = self.game.get_grid().get_mapping()
            inventory = self.game.get_player().get_inventory()
            game_record = GameRecord(path)
            game_record.save(mapping, inventory, self.game.get_steps(), self.time_counter)

    def _load_game(self, path):
        """
        Load game from saves.
        :return:
        """
        global BEGIN_COUNT_TIME
        game_record = GameRecord(path)
        reload_game(game=self.game, grid=game_record.mapping)
        for i, lt in game_record.inventory:
            entity = AdvancedMapLoader().create_entity(i)
            entity._lifetime = int(lt)
            self.game.get_player().get_inventory().add_item(entity)
        self.game._steps = int(game_record.steps)
        self.time_counter = int(game_record.timer)

    def _quit_game(self):
        """
        Quit the game.
        :return:
        """
        self.root.quit()

    def _popup_high_scores(self):
        """
        Popup high scores window.
        """
        if self.high_scores_popup:
            self.high_scores_popup.destroy()
        self.high_scores_popup = tk.Toplevel()
        self.high_scores_popup.attributes('-toolwindow', 1)
        self.high_scores_popup.wm_attributes("-topmost", 1)
        self.high_scores_popup.focus_set()
        screenwidth = self.root.winfo_screenwidth()
        screenheight = self.root.winfo_screenheight()
        self.high_scores_popup.geometry(f'{self.high_scores_width}x{self.high_scores_height}+'
                                        f'{(screenwidth - self.high_scores_width - self.window_width + self.size * CELL_SIZE) // 2}+'
                                        f'{(screenheight - self.high_scores_height) // 2}')

        self.high_scores_popup.title('TOP 3'.rjust(30))
        tk.Label(self.high_scores_popup, text='High Scores', font=('Arial', 24, 'bold'), bg=DARKEST_PURPLE,
                 fg='white').pack(
            side=tk.TOP, fill=tk.X)
        # show scores

        high_scores = load_high_scores()
        for item in high_scores:
            item[1] = f'{item[1] // 60}m {item[1] % 60}s'
            tk.Label(self.high_scores_popup, text=': '.join(item), font=('Arial', 10, 'normal')).pack(side=tk.TOP,
                                                                                                      pady=2)

        tk.Button(self.high_scores_popup, text='Done', font=('Arial', 10, 'normal'),
                  command=self.high_scores_popup.destroy).pack(
            side=tk.BOTTOM, pady=2)

    def _create_menu(self):
        """
        Create file menu.
        :return:
        """
        menubar = tk.Menu(self.root)
        menubar.add_command(label='Restart game', command=self._restart_game)

        # save game menu
        saves = tk.Menu(self.root, tearoff=False)
        saves.add_command(label='New Save', command=lambda: print('create New save'))
        saves.add_separator()
        for i in self.game_records:
            saves.add_command(label=i.split('\\')[1], command=lambda p=i: self._save_game(p))
        menubar.add_cascade(label='Save game', menu=saves)

        # load game menu
        load_saves = tk.Menu(self.root, tearoff=False)
        load_saves.add_separator()
        for i in self.game_records:
            load_saves.add_command(label=i.split('\\')[1], command=lambda p=i: self._load_game(p))
        menubar.add_cascade(label='Load game', menu=load_saves)
        menubar.add_command(label='Quit', command=self._quit_game)

        # High Scores
        menubar.add_command(label='High Scores', command=self._popup_high_scores)

        self.root['menu'] = menubar

    def _save_high_scores(self):
        """
        Save high scores to file.
        """
        player_name = self.player_name_input.get()
        if player_name:
            self.high_scores.append((player_name, self.final_score))
            self.high_scores.sort(key=lambda x: int(x[1]))
            with open(HIGH_SCORES_FILE, 'w', encoding='utf-8') as f:
                for item in self.high_scores[:3]:
                    f.write(': '.join((item[0], str(item[1]))))
                    f.write('\n')

            self.prompt_popup.destroy()

    def _save_high_scores_and_play_again(self):
        """
        Save high scores and restart game.
        """
        player_name = self.player_name_input.get()
        if player_name:
            self._save_high_scores()
            self._restart_game()

    def draw(self, game: Game):
        """
        Draw game
        :param game:
        :return:
        """
        self._create_menu()
        screenwidth = self.root.winfo_screenwidth()
        screenheight = self.root.winfo_screenheight()
        self.root.geometry(f'{self.window_width}x{self.window_height}+'
                           f'{(screenwidth - self.window_width) // 2}+{(screenheight - self.window_height) // 2}')
        self._create_banner(self.root)
        self._create_status_bar(self.root, game)
        self._create_game_map(self.root, game)
        self._create_inventory(self.root, game)
        # TODO: File menu

    def popup_prompt(self):
        global WIN_OR_LOSE, PROMPT_POPUP
        self.prompt_popup = tk.Toplevel()
        PROMPT_POPUP = self.prompt_popup
        # always on the top
        # self.prompt.wm_attributes("-topmost", 1)
        self.prompt_popup.attributes('-toolwindow', 1)
        self.prompt_popup.focus_set()
        screenwidth = self.root.winfo_screenwidth()
        screenheight = self.root.winfo_screenheight()
        self.prompt_popup.geometry(f'{self.prompt_width}x{self.prompt_height}+'
                                   f'{(screenwidth - self.prompt_width - self.window_width + self.size * CELL_SIZE) // 2}+'
                                   f'{(screenheight - self.prompt_height) // 2}')
        if WIN_OR_LOSE:
            self.prompt_popup.title(WIN_MESSAGE)
            # if self.time_counter < self.high_scores[-1]:
            first_line = f'You won in {self.time_counter // 60}m and {self.time_counter % 60}s!'
        else:
            self.prompt_popup.title(LOSE_MESSAGE)
            first_line = f'You lost in {self.time_counter // 60}m and {self.time_counter % 60}s!'

        tk.Label(self.prompt_popup, text=first_line).pack(side=tk.TOP, fill=tk.BOTH, expand=True)
        self.final_score = self.time_counter
        if WIN_OR_LOSE:
            if len(self.high_scores) < 3 or self.final_score < self.high_scores[-1][1]:
                first_line += ' Enter your name:'
                self.player_name_input = tk.Entry(self.prompt_popup)
                self.player_name_input.pack(side=tk.TOP, pady=3)
                self.player_name_input.focus_set()

                button_enter = tk.Button(self.prompt_popup, text='Enter', command=self._save_high_scores)
                button_enter.pack(side=tk.LEFT, expand=True, pady=3)
                button_enter_and_play_again = tk.Button(self.prompt_popup, text='Enter and play again',
                                                        command=self._save_high_scores_and_play_again)
                button_enter_and_play_again.pack(side=tk.LEFT, expand=True, pady=3)
        else:
            button_enter = tk.Button(self.prompt_popup, text='Play again', command=self._restart_game)
            button_enter.pack(side=tk.LEFT, expand=True, pady=3)
            button_enter_and_play_again = tk.Button(self.prompt_popup, text='Quit game',
                                                    command=self._quit_game)
            button_enter_and_play_again.pack(side=tk.LEFT, expand=True, pady=3)

    def _move(self, game, direction):
        """

        :param game:
        :param direction:
        :return:
        """
        global WIN_OR_LOSE, RESTARTING, BEGIN_COUNT_TIME, CAN_MOVE
        if RESTARTING:
            return
        if not BEGIN_COUNT_TIME and WIN_OR_LOSE is None:
            BEGIN_COUNT_TIME = True
        if not CAN_MOVE:
            return
        offset = game.direction_to_offset(direction)
        if offset is not None:
            game.move_player(offset)
            game.step()
        self.status_bar.moves_counter.set(f'{game.get_steps()} moves')
        self._create_game_map(self.root, game)

        if game.has_won():
            print(WIN_MESSAGE)
            WIN_OR_LOSE = 1
            self.popup_prompt()
            BEGIN_COUNT_TIME = False
            CAN_MOVE = False
        if not game.has_won() and game.has_lost():
            print(LOSE_MESSAGE)
            WIN_OR_LOSE = 0
            self.popup_prompt()
            BEGIN_COUNT_TIME = False
            CAN_MOVE = False

    def _step(self, game):
        """

        :param game:
        :return:
        """
        global WIN_OR_LOSE
        if BEGIN_COUNT_TIME:
            self.time_counter += 1
        # when step == 0 (no move), timer counter must be 0
        if self.game.get_steps() == 0:
            self.time_counter = 0
        if WIN_OR_LOSE is None:
            self.status_bar.timer.set(f'{self.time_counter // 60} mins {self.time_counter % 60} seconds')
            self.status_bar.moves_counter.set(f'{game.get_steps()} moves')
        super()._step(game)

    def play(self, game):
        """
        Binds events and initialises gameplay.
        :param game: An instance of the game class that is to be displayed
                     to the user by printing the grid.
        :return:
        """
        global SAVED_GRID
        SAVED_GRID = game.get_grid().serialize()
        self.game = game
        self.draw(game)
        self.root.focus_set()
        self.root.bind('<KeyPress >', lambda e: self.handle_action(e, game))
        self.root.after(2000, self._step(game))
        self.root.mainloop()
